using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Sound_landing : MonoBehaviour
{
    [FMODUnity.EventRef] public string landingevent;
    public vThirdPersonInput vikingInput;

    void Start()
    {
        vikingInput = GetComponent<vThirdPersonInput>();
    }

    void landing()

        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(landingevent, gameObject);
        }

    
}