using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Sound_footsteps_surface : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string walkingEvent;

    [FMODUnity.EventRef]
    public string runningEvent;

    vThirdPersonInput tpInput;
    FMOD.Studio.EventInstance walkingInstance;
    vThirdPersonController tpController;

 
    [FMODUnity.EventRef]
    public string landEvent;


    FMOD.Studio.EventInstance landInstance;

    public LayerMask lm;
    float surface;



    private void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }

     
    void footsteps()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            SurfaceCheck();
            if (tpController.isJumping == false)
            { 

                if (tpController.isSprinting)
                {
                    walkingInstance = FMODUnity.RuntimeManager.CreateInstance(runningEvent);
                    FMODUnity.RuntimeManager.AttachInstanceToGameObject(walkingInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
                    walkingInstance.setParameterByName("surface_type", surface);
                    walkingInstance.start();
                    walkingInstance.release();
                }

                else
                {
                    walkingInstance = FMODUnity.RuntimeManager.CreateInstance(walkingEvent);
                    FMODUnity.RuntimeManager.AttachInstanceToGameObject(walkingInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
                    walkingInstance.setParameterByName("surface_type", surface);
                    walkingInstance.start();
                    walkingInstance.release();
                }

          
        }

    }

    
}
void landing()
{
    landInstance = FMODUnity.RuntimeManager.CreateInstance(landEvent);
    landInstance.setParameterByName("locomotion_type", 4f);
    landInstance.setParameterByName("surface_type", surface);
    FMODUnity.RuntimeManager.AttachInstanceToGameObject(landInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
    landInstance.start();
    landInstance.release();
}

    void SurfaceCheck()
{

    if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 0.5f, lm))
    {
        Debug.Log(hit.collider.tag);

        switch (hit.collider.tag)
        {
            case "vSnow":
                surface = 0f;
                break;
            case "vWood":
                surface = 1f;
                break;
            case "vStone":
                surface = 2f;
                break;
            case "vWater":
                surface = 3f;
                break;
            case "vGround":
                    surface = 4f;
                    break;
            default:
                surface = 4f;
                break;
        }

    }

}

}