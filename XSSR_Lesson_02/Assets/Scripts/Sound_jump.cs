using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Sound_jump : MonoBehaviour
{
    [FMODUnity.EventRef] public string jumpevent;
    public vThirdPersonInput vikingInput;

    void Start()
    {
        vikingInput = GetComponent<vThirdPersonInput>();
    }

    void jump()

    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(jumpevent, gameObject);
    }


}
