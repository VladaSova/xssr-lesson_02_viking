using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class Footsteps_Collider : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string footstepsevent;
    public float tipPoverhnosti;
    public vThirdPersonInput tpInput;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("vGround"))
        {
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            //eventInstance.setParameterByName("isRuning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", 4);
            eventInstance.start();
            eventInstance.release();
        }
        else if (other.CompareTag("vWood"))
        {
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            //eventInstance.setParameterByName("isRuning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", 1);
            eventInstance.start();
            eventInstance.release();
        }
        else if (other.CompareTag("vWood"))
        {
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            //eventInstance.setParameterByName("isRuning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", 1);
            eventInstance.start();
            eventInstance.release();
        }
        else if (other.CompareTag("vWater"))
        {
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            //eventInstance.setParameterByName("isRuning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", 3);
            eventInstance.start();
            eventInstance.release();
        }
        else if (other.CompareTag("vStone"))
        {
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            //eventInstance.setParameterByName("isRuning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", 2);
            eventInstance.start();
            eventInstance.release();
        }

        else if (other.CompareTag("vSnow"))
        {
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            //eventInstance.setParameterByName("isRuning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", 0);
            eventInstance.start();
            eventInstance.release();
        }
    }
}

