using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    public GameObject[] waypoints;
    public int num = 0;

    public float minDist;
    public float speed;

    public bool go = true;


    void Update()
    {
        float dist = Vector3.Distance(gameObject.transform.position, waypoints[num].transform.position);

        if (go)

        {
            if (dist > minDist)
            {
                Move();
            }
            else
            {
                if (num +1 == waypoints.Length)
                {
                    num = 0;
                }
                else
                {
                    num++;

                }
            }
        }
    }
    public void Move()
    {
        gameObject.transform.position += gameObject.transform.forward * speed * Time.deltaTime;
    }
}
