using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Sound_footsteps : MonoBehaviour
{
    [FMODUnity.EventRef] public string footstepsevent;
    public vThirdPersonInput vikingInput;

    void Start()
    {
        vikingInput = GetComponent<vThirdPersonInput>();
    }

    void Update()
    {
        
    }

    void footsteps()

    {
        if (vikingInput.cc.inputMagnitude > 0.1)

        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(footstepsevent, gameObject);
        }
    
    }
}
