using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound_torch : MonoBehaviour
{
    [FMODUnity.EventRef] public string village_props_torchevent;

    FMOD.Studio.EventInstance village_props_torchinstance;


    void Start()
    {
        village_props_torchinstance = FMODUnity.RuntimeManager.CreateInstance(village_props_torchevent);
        //village_props_torchinstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(village_props_torchinstance, gameObject.GetComponent<Transform>(), gameObject.GetComponent<Rigidbody>());

        village_props_torchinstance.start();

        //FMODUnity.RuntimeManager.PlayOneShot(village_props_torch, gameObject.transform.position);
    }

   
    void Update()
    {
        
    }
}
